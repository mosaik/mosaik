==================================
Welcome to mosaik's documentation!
==================================

mosaik is a flexible smart-grid co-simulation framework.

mosaik allows you to reuse and combine existing simulation models and simulators to create large-scale smart grid scenarios – and by large-scale we mean thousands of simulated entities distributed over multiple simulator processes.
These scenarios can then serve as a test bed for various types of control strategies (e.g., multi-agent systems (MAS) or centralized control).

Here, we provide the documentation about mosaik.

.. toctree::
   :caption: Contents
   :maxdepth: 2

   overview
   installation
   tutorials/index
   api_reference/index
   ecosystem/index
   mosaik-api/index
   how-tos/index
   explanations/index
   faq
   dev/index
   about/index
   privacy
   legals
   datenschutz
   impressum
   glossary
   genindex



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
