================================================================
``mosaik.scenario`` --- Classes related to the scenario creation
================================================================

.. currentmodule:: mosaik.scenario

.. automodule:: mosaik.scenario

.. autodata:: SimConfig


.. autoclass:: World
   :members:

.. autoclass:: ModelFactory
   :members:

.. autoclass:: ModelMock
   :members:

.. autoclass:: Entity
   :members:
