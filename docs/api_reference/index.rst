======================
Scenario API Reference
======================

This is the reference for mosaik's scenario API, meaning the set of classes and methods that you use to write a scenario (as opposed to connecting a new tool or programming language to mosaik, which would be covered by the :doc:`simulator API </mosaik-api/index>`).

This reference should also be helpful if you want to work on or extend mosaik itself.


.. toctree::

   mosaik
   mosaik.exceptions
   mosaik.scenario
   mosaik.async_scenario
   mosaik.scheduler
   mosaik.simmanager
   mosaik.util
   mosaik.basic_simulators
