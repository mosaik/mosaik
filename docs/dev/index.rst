=========================
Developer's Documentation
=========================

On these pages, we collect information that is mostly relevant for people who work on mosaik itself.
If you are a user of mosaik, this information is probably not of interest to you.

.. toctree::
   :caption: Contents
   :maxdepth: 1

   /simmanager
   design-decisions
   logo-ci
