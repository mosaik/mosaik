==========
How to ...
==========

On this page, you will find a selection of how-to guides on how to use mosaik and on more specific applications of mosaik


..., as a scenario author, ...
==============================

.. toctree::
   :maxdepth: 1

   ... write a scenario </scenario-definition>
   ... simulate at different time scales </tutorials/sametimeloops>
   ... debug a mosaik simulation <debugging>
   ... use mosaik in a Jupyter notebook </tutorials/jupyter_notebook>
   ... visualize your results with Apache Superset </tutorials/apache-superset>
   ... upgrade from mosaik 2 </upgrade_to_v3>
   ... pause and resume a mosaik simulation <pause-resume>
   ... convert units of data flowing from one simulator to another <transform-functions>

..., as a simulator author, ...
===============================

.. toctree::
   :maxdepth: 1

   ... use existing topologies via child entities <existing-topologies>
   ... avoid unnecessary simulator steps using max_advance <max-advance>
   ... work with events that occur in-between steps </tutorials/set-external-events>


..., as an Odysseus user, ...
=============================

.. toctree::
   :maxdepth: 1

   ... connect mosaik to Odysseus </tutorials/odysseus>
