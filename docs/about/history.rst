=====================
The history of mosaik
=====================

Our work on mosaik started on July 15th, 2010 -- at least, the initial
commit happened on that day. Since then, we've come a long way …

.. include:: ../../CHANGES.rst
